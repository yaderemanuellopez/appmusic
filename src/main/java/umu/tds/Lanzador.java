package umu.tds;

import java.awt.EventQueue;

import umu.tds.gui.LoginView;
import umu.tds.gui.VentanaPrincipal;

public class Lanzador {
	public static void main(final String[] args){
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginView ventana = new LoginView();
					//VentanaPrincipal ventana = new VentanaPrincipal();
					ventana.mostrarVentana();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}