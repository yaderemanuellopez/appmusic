package umu.tds.controlador;

import umu.tds.dao.UsuarioDAO;

import java.util.EventObject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import umu.tds.componente.CancionesEvent;
import umu.tds.componente.CancionesListener;
import umu.tds.componente.CargadorCanciones;
import umu.tds.dao.CancionDAO;
import umu.tds.dao.DAOException;
import umu.tds.dao.FactoriaDAO;
import umu.tds.dominio.Usuario;
import umu.tds.dominio.Cancion;
import umu.tds.dominio.CatalogoCanciones;
import umu.tds.dominio.CatalogoUsuarios;
import umu.tds.dominio.ListaCanciones;

public final class Controlador implements CancionesListener{

	private Usuario usuarioActual;
	private static Controlador unicaInstancia;
	private FactoriaDAO factoria;
	private CargadorCanciones cargadorDeCanciones;

	private Controlador() {
		usuarioActual = null;
		try {
			factoria = FactoriaDAO.getInstancia();
			cargadorDeCanciones = new CargadorCanciones();
		} catch (DAOException e) {
			e.printStackTrace();
		}
	}

	public static Controlador getUnicaInstancia() {
		if (unicaInstancia == null)
			unicaInstancia = new Controlador();
		return unicaInstancia;
	}

	public Usuario getUsuarioActual() {
		return usuarioActual;
	}

	public boolean esUsuarioRegistrado(String login) {
		return CatalogoUsuarios.getUnicaInstancia().getUsuario(login) != null;
	}

	public boolean loginUsuario(String nombre, String password) {
		Usuario usuario = CatalogoUsuarios.getUnicaInstancia().getUsuario(nombre);
		if (usuario != null && usuario.getPassword().equals(password)) {
			this.usuarioActual = usuario;
			return true;
		}
		return false;
	}

	public boolean registrarUsuario(String nombre, String apellidos, String email, String login, String password,
			String fechaNacimiento) {

		if (esUsuarioRegistrado(login))
			return false;
		Usuario usuario = new Usuario(nombre, apellidos, email, login, password, fechaNacimiento);

		UsuarioDAO usuarioDAO = factoria
				.getUsuarioDAO(); /* Adaptador DAO para almacenar el nuevo Usuario en la BD */
		usuarioDAO.create(usuario);

		CatalogoUsuarios.getUnicaInstancia().addUsuario(usuario);
		return true;
	}

	public boolean borrarUsuario(Usuario usuario) {
		if (!esUsuarioRegistrado(usuario.getLogin()))
			return false;

		UsuarioDAO usuarioDAO = factoria.getUsuarioDAO(); /* Adaptador DAO para borrar el Usuario de la BD */
		usuarioDAO.delete(usuario);

		CatalogoUsuarios.getUnicaInstancia().removeUsuario(usuario);
		return true;
	}
	
	
	/*************************Metodos de Canciones****************************/
	
	public boolean esCancionRegistrada(String titulo) {
		
		return CatalogoCanciones.getUnicaInstancia().getCancion(titulo) != null;
	}
	
	public boolean registrarCancion(String titulo, String interprete, String estilo, String URL) {
		if(esCancionRegistrada(titulo))
			return false;
		Cancion cancion = new Cancion(titulo, interprete, estilo, URL);
		CancionDAO cancionDAO = factoria.getCancionDAO();/* Adaptador DAO para almacenar la nueva cancion  en la BD */
		cancionDAO.create(cancion);
		
		CatalogoCanciones.getUnicaInstancia().addCancion(cancion);
		return true;
	}
	
	public boolean borrarCancion(Cancion cancion) {
		if(!esCancionRegistrada(cancion.getTitulo()))
			return false;
		CancionDAO cancionDAO = factoria.getCancionDAO();/* Adaptador DAO para borrar la cancion de la BD */
		cancionDAO.delete(cancion);
		
		CatalogoCanciones.getUnicaInstancia().removeCancion(cancion);
		return true;
	}
	
	public List<String> getEstilosMusicales(){
		return factoria.getCancionDAO().getAll().stream()
				.map(c -> c.getEstilo())
				.distinct()
				.collect(Collectors.toList());
	}
	
	public List<Cancion> getCacionesporfiltro(String titulo,String interprete,String estilo){
		List<Cancion> canciones = new LinkedList<Cancion>();

		if(titulo.isEmpty() && interprete.isEmpty() && !estilo.isEmpty())
			canciones = factoria.getCancionDAO().getAll().stream()
			.filter(c -> c.getEstilo().equals(estilo))
			.collect(Collectors.toList());
		
		if(!titulo.isEmpty() && interprete.isEmpty() && !estilo.isEmpty())
			canciones = factoria.getCancionDAO().getAll().stream()
			.filter(c -> c.getTitulo().equals(titulo) && c.getEstilo().equals(estilo))
			.collect(Collectors.toList());
		
		if(titulo.isEmpty() && !interprete.isEmpty() && !estilo.isEmpty())
			canciones = factoria.getCancionDAO().getAll().stream()
			.filter(c -> c.getInterprete().equals(interprete) && c.getEstilo().equals(estilo))
			.collect(Collectors.toList());
		
		if(!titulo.isEmpty() && interprete.isEmpty() && estilo.isEmpty())
			canciones = factoria.getCancionDAO().getAll().stream()
			.filter(c -> c.getTitulo().equals(titulo))
			.collect(Collectors.toList());
		
		if(titulo.isEmpty() && !interprete.isEmpty() && estilo.isEmpty())
			canciones = factoria.getCancionDAO().getAll().stream()
			.filter(c -> c.getInterprete().equals(interprete))
			.collect(Collectors.toList());
		
		if(!titulo.isEmpty() && !interprete.isEmpty() && estilo.isEmpty())
			canciones = factoria.getCancionDAO().getAll().stream()
			.filter(c -> c.getTitulo().equals(titulo) &&  c.getInterprete().equals(interprete))
			.collect(Collectors.toList());
		
		if(!titulo.isEmpty() && !interprete.isEmpty() && !estilo.isEmpty())
			canciones = factoria.getCancionDAO().getAll().stream()
			.filter(c -> c.getTitulo().equals(titulo) && c.getEstilo().equals(estilo) && c.getInterprete().equals(interprete))
			.collect(Collectors.toList());
		
		return canciones;
	}
	
	public List<Cancion> getAllcanciones(){
		return factoria.getCancionDAO().getAll();
	}
	
	/*************************Metodos de Lista de Canciones****************************/
	
	public ListaCanciones NuevaListaVacia(String nombreNuevaLista) {
		if(!existeListaPorNombre(nombreNuevaLista))
			return usuarioActual.addNuevaListaVacia(nombreNuevaLista);
		return usuarioActual.getListaCancionesPorNombre(nombreNuevaLista);
		
	}
	
	public boolean existeListaPorNombre(String nombreLista) {
		return usuarioActual.existeListaPorNombre(nombreLista);
	}
	
	public void NuevaLista(ListaCanciones listaNueva) {
		if(!existeListaPorNombre(listaNueva.getNombre())) {
			usuarioActual.addNuevaLista(listaNueva);
			UsuarioDAO usuarioDAO = factoria.getUsuarioDAO();
			usuarioDAO.updateListas(usuarioActual);
		}
		//TODO hacer que se modifique una lista ya existente
	}
	
	public List<ListaCanciones> getMisListas() {
		return usuarioActual.getListaCanciones();
	}
	
 /***********************************implementacion interfaz componente cargador de canciones**************************/
	@Override
	public void nuevasCanciones(EventObject ev) {
		if(ev instanceof CancionesEvent){ 
			CancionesEvent evento = (CancionesEvent) ev;
			for ( umu.tds.componente.Cancion c : evento.getCanciones()) {
				registrarCancion(c.getTitulo(), c.getInterprete(), c.getEstilo(), c.getURL());
			}
		}
	}
	
	/*********************************Metodo para cargar las canciones******************************************************************************/
	public void cargarCanciones(String rutaArchivo) {
		cargadorDeCanciones.addCancionListener(this);
		cargadorDeCanciones.setArchivoCanciones(rutaArchivo);
		
	}

	
	/**********************datos tablas********************/

}
