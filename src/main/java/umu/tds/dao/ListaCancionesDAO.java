package umu.tds.dao;

import java.util.List;

import umu.tds.dominio.ListaCanciones;

public interface ListaCancionesDAO {

	void create(ListaCanciones listaCanciones);
	boolean delete(ListaCanciones listaCanciones);
	void updateLista(ListaCanciones listaCanciones);
	ListaCanciones get(int id);
	List<ListaCanciones> getAll();
}
