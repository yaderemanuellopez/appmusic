package umu.tds.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import beans.Entidad;
import beans.Propiedad;
import tds.driver.FactoriaServicioPersistencia;
import tds.driver.ServicioPersistencia;
import umu.tds.dominio.Cancion;

/**
 * 
 * Clase que implementa el Adaptador DAO concreto de Cancion para el tipo H2.
 * 
 */
public class TDSCancionDAO implements CancionDAO{

	private static final String CANCION = "Cancion";
	
	private static final String TITULO = "Titulo";
	private static final String INTERPRETE = "Interprete";
	private static final String ESTILO = "Estilo";
	private static final String URL = "URL";
	
	private ServicioPersistencia servPersistencia;
	
	
	public TDSCancionDAO() {
		servPersistencia = FactoriaServicioPersistencia.getInstance().getServicioPersistencia();
	}
	
	private Cancion entidadToCancion(Entidad eCancion) {
		
		String titulo = servPersistencia.recuperarPropiedadEntidad(eCancion, TITULO);
		String interprete = servPersistencia.recuperarPropiedadEntidad(eCancion, INTERPRETE);
		String estilo = servPersistencia.recuperarPropiedadEntidad(eCancion, ESTILO);
		String Url = servPersistencia.recuperarPropiedadEntidad(eCancion, URL);
		
		Cancion cancion = new Cancion(titulo, interprete, estilo, Url);
		cancion.setId(eCancion.getId());
		return cancion;
	}
	
	private Entidad cancionToEntidad(Cancion cancion) {
		Entidad eCancion = new Entidad();
		eCancion.setNombre(CANCION);
		
		eCancion.setPropiedades(new ArrayList<Propiedad>(Arrays.asList(
				new Propiedad(TITULO,cancion.getTitulo()),
				new Propiedad(INTERPRETE,cancion.getInterprete()),
				new Propiedad(ESTILO,cancion.getEstilo()),
				new Propiedad(URL,cancion.getURL()))));
		
		return eCancion;
	} 
	
	@Override
	public void create(Cancion cancion) {
		Entidad eCancion = this.cancionToEntidad(cancion);
		eCancion = servPersistencia.registrarEntidad(eCancion);
		cancion.setId(eCancion.getId());
		
	}

	@Override
	public boolean delete(Cancion cancion) {
		Entidad eCancion;
		eCancion = servPersistencia.recuperarEntidad(cancion.getId());
		
		return servPersistencia.borrarEntidad(eCancion);
	}

	@Override
	public Cancion get(int id) {
		if(PoolDAO.getUnicaInstancia().contiene(id))
			return (Cancion) PoolDAO.getUnicaInstancia().getObjeto(id);
		
		Entidad eCancion = servPersistencia.recuperarEntidad(id);
		Cancion cancion = entidadToCancion(eCancion);
		return  cancion;
	}

	@Override
	public List<Cancion> getAll() {
		List<Entidad> entidades = servPersistencia.recuperarEntidades(CANCION);

		List<Cancion> canciones = new LinkedList<Cancion>();
		for (Entidad eUsuario : entidades) {
			canciones.add(get(eUsuario.getId()));
		}
		
		return canciones;
	}

}
