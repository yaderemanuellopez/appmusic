package umu.tds.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import beans.Entidad;
import beans.Propiedad;
import tds.driver.FactoriaServicioPersistencia;
import tds.driver.ServicioPersistencia;
import umu.tds.dominio.Cancion;
import umu.tds.dominio.ListaCanciones;

public class TDSListaCancionesDAO implements ListaCancionesDAO {

	private static final String LISTACANCIONES = "listaCanciones";
	
	private static final String NOMBRE = "nombre";
	private static final String CANCIONES = "canciones";
	
	
	private ServicioPersistencia servPersistencia;

	public TDSListaCancionesDAO() {
		servPersistencia = FactoriaServicioPersistencia.getInstance().getServicioPersistencia();
	}

	
	private ListaCanciones entidadToListaCanciones(Entidad eListaCanciones) {

		String nombre = servPersistencia.recuperarPropiedadEntidad(eListaCanciones, NOMBRE);
		String canciones = servPersistencia.recuperarPropiedadEntidad(eListaCanciones, CANCIONES);

		ListaCanciones listaCanciones = new ListaCanciones(nombre);
		listaCanciones.setId(eListaCanciones.getId());
		if(canciones != null)
			listaCanciones.setCanciones(obtenerCancionesDesdeCodigos(canciones));
		
		return listaCanciones;
	}
	
	private Entidad listaCancionesToEntidad(ListaCanciones listaCanciones) {
		Entidad eListaCanciones = new Entidad();
		eListaCanciones.setNombre(LISTACANCIONES);

		eListaCanciones.setPropiedades(new ArrayList<Propiedad>(Arrays.asList(
				new Propiedad(NOMBRE, listaCanciones.getNombre()),
				new Propiedad(CANCIONES, obtenerCodigosCanciones(listaCanciones.getCanciones()))
				)));
		return eListaCanciones;
	}
	
	@Override
	public void create(ListaCanciones listaCanciones) {
		Entidad eListaCanciones = this.listaCancionesToEntidad(listaCanciones);
		eListaCanciones = servPersistencia.registrarEntidad(eListaCanciones);
		listaCanciones.setId(eListaCanciones.getId());

	}

	@Override
	public boolean delete(ListaCanciones listaCanciones) {
		Entidad eListaCanciones;
		eListaCanciones = servPersistencia.recuperarEntidad(listaCanciones.getId());
		
		return servPersistencia.borrarEntidad(eListaCanciones);
	}

	@Override
	public void updateLista(ListaCanciones listaCanciones) {
		Entidad eListaCanciones = servPersistencia.recuperarEntidad(listaCanciones.getId());
		servPersistencia.eliminarPropiedadEntidad(eListaCanciones, NOMBRE);
		servPersistencia.anadirPropiedadEntidad(eListaCanciones, NOMBRE, listaCanciones.getNombre());
		servPersistencia.eliminarPropiedadEntidad(eListaCanciones, CANCIONES);
		servPersistencia.anadirPropiedadEntidad(eListaCanciones, CANCIONES, obtenerCodigosCanciones(listaCanciones.getCanciones()));
	}

	@Override
	public ListaCanciones get(int id) {
		if(PoolDAO.getUnicaInstancia().contiene(id))
			return (ListaCanciones) PoolDAO.getUnicaInstancia().getObjeto(id);
		
		Entidad eListaCanciones = servPersistencia.recuperarEntidad(id);
		ListaCanciones listaCanciones = entidadToListaCanciones(eListaCanciones);
		PoolDAO.getUnicaInstancia().addObjeto(eListaCanciones.getId(), listaCanciones);
		
		return listaCanciones;
	}

	@Override
	public List<ListaCanciones> getAll() {
		List<Entidad> entidades = servPersistencia.recuperarEntidades(LISTACANCIONES);

		List<ListaCanciones> listaCanciones = new LinkedList<ListaCanciones>();
		for (Entidad eListaCanciones : entidades) {
			listaCanciones.add(get(eListaCanciones.getId()));
		}
		
		return listaCanciones;
	}
	
	
	// -------------------Funciones auxiliares-----------------------------
		private String obtenerCodigosCanciones(List<Cancion> canciones) {
			String lineas = "";
			for (Cancion cancion : canciones) {
				lineas += cancion.getId() + " ";
			}
			return lineas.trim();

		}

		private List<Cancion> obtenerCancionesDesdeCodigos(String lineas) {

			List<Cancion> canciones = new LinkedList<Cancion>();
			StringTokenizer strTok = new StringTokenizer(lineas, " ");
			CancionDAO cancionDAO;
			try {
				cancionDAO = TDSFactoriaDAO.getInstancia().getCancionDAO();
				while (strTok.hasMoreTokens()) {
					canciones.add(cancionDAO.get(Integer.valueOf((String) strTok.nextElement())));
				}
			} catch (DAOException e) {
				e.printStackTrace();
			}
			

			return canciones;
		}

}
