package umu.tds.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import tds.driver.FactoriaServicioPersistencia;
import tds.driver.ServicioPersistencia;
import umu.tds.dominio.ListaCanciones;
import umu.tds.dominio.Usuario;
import beans.Entidad;
import beans.Propiedad;

/**
 * 
 * Clase que implementa el Adaptador DAO concreto de Usuario para el tipo H2.
 * 
 */
public final class TDSUsuarioDAO implements UsuarioDAO {

	private static final String USUARIO = "Usuario";
	
	private static final String NOMBRE = "nombre";
	private static final String APELLIDOS = "apellidos";
	private static final String EMAIL = "email";
	private static final String LOGIN = "login";
	private static final String PASSWORD = "password";
	private static final String FECHA_NACIMIENTO = "fechaNacimiento";
	private static final String ES_PREMIUM = "esPremium";
	private static final String LISTACANCIONES = "listaCanciones";
	
	private ServicioPersistencia servPersistencia;
	

	public TDSUsuarioDAO() {
		servPersistencia = FactoriaServicioPersistencia.getInstance().getServicioPersistencia();
	}

	private Usuario entidadToUsuario(Entidad eUsuario) {

		String nombre = servPersistencia.recuperarPropiedadEntidad(eUsuario, NOMBRE);
		String apellidos = servPersistencia.recuperarPropiedadEntidad(eUsuario, APELLIDOS);
		String email = servPersistencia.recuperarPropiedadEntidad(eUsuario, EMAIL);
		String login = servPersistencia.recuperarPropiedadEntidad(eUsuario, LOGIN);
		String password = servPersistencia.recuperarPropiedadEntidad(eUsuario, PASSWORD);
		String fechaNacimiento = servPersistencia.recuperarPropiedadEntidad(eUsuario, FECHA_NACIMIENTO);
		String esPremium = servPersistencia.recuperarPropiedadEntidad(eUsuario, ES_PREMIUM);
		String listaCanciones = servPersistencia.recuperarPropiedadEntidad(eUsuario, LISTACANCIONES);

		Usuario usuario = new Usuario(nombre, apellidos, email, login, password, fechaNacimiento);
		usuario.setId(eUsuario.getId());
		usuario.setListaCanciones(obtenerListaCancionesDesdeCodigos(listaCanciones));
		usuario.setEsPremium(esTrueOrFalse(esPremium));
		
		return usuario;
	}

	private Entidad usuarioToEntidad(Usuario usuario) {
		Entidad eUsuario = new Entidad();
		eUsuario.setNombre(USUARIO);

		eUsuario.setPropiedades(new ArrayList<Propiedad>(Arrays.asList(
				new Propiedad(NOMBRE, usuario.getNombre()),
				new Propiedad(APELLIDOS, usuario.getApellidos()), 
				new Propiedad(EMAIL, usuario.getEmail()),
				new Propiedad(LOGIN, usuario.getLogin()), 
				new Propiedad(PASSWORD, usuario.getPassword()),
				new Propiedad(FECHA_NACIMIENTO, usuario.getFechaNacimiento()),
				new Propiedad(ES_PREMIUM, StringTrueOrFalse(usuario.isEsPremium())),
				new Propiedad(LISTACANCIONES,obtenerCodigosListaCanciones(usuario.getListaCanciones())))));
		return eUsuario;
	}

	public void create(Usuario usuario) {
		Entidad eUsuario = this.usuarioToEntidad(usuario);
		eUsuario = servPersistencia.registrarEntidad(eUsuario);
		usuario.setId(eUsuario.getId());
	}

	public boolean delete(Usuario usuario) {
		Entidad eUsuario;
		eUsuario = servPersistencia.recuperarEntidad(usuario.getId());
		
		return servPersistencia.borrarEntidad(eUsuario);
	}

	/**
	 * Permite que un Usuario modifique su perfil: password y email
	 */
	public void updatePerfil(Usuario usuario) {
		Entidad eUsuario = servPersistencia.recuperarEntidad(usuario.getId());
		servPersistencia.eliminarPropiedadEntidad(eUsuario, PASSWORD);
		servPersistencia.anadirPropiedadEntidad(eUsuario, PASSWORD, usuario.getPassword());
		servPersistencia.eliminarPropiedadEntidad(eUsuario, EMAIL);
		servPersistencia.anadirPropiedadEntidad(eUsuario, EMAIL, usuario.getEmail());
	}

	@Override
	public void updateListas(Usuario usuario) {
		Entidad eUsuario = servPersistencia.recuperarEntidad(usuario.getId());
		servPersistencia.eliminarPropiedadEntidad(eUsuario, LISTACANCIONES);
		servPersistencia.anadirPropiedadEntidad(eUsuario, LISTACANCIONES, obtenerCodigosListaCanciones(usuario.getListaCanciones()));	
	}
	
	@Override
	public void updatePremium(Usuario usuario) {
		Entidad eUsuario = servPersistencia.recuperarEntidad(usuario.getId());
		servPersistencia.eliminarPropiedadEntidad(eUsuario, ES_PREMIUM);
		servPersistencia.anadirPropiedadEntidad(eUsuario, ES_PREMIUM, StringTrueOrFalse(usuario.isEsPremium()));
	}
	
	public Usuario get(int id) {
		if(PoolDAO.getUnicaInstancia().contiene(id))
			return (Usuario) PoolDAO.getUnicaInstancia().getObjeto(id);
		
		Entidad eUsuario = servPersistencia.recuperarEntidad(id);
		
		Usuario usuario = entidadToUsuario(eUsuario);
		PoolDAO.getUnicaInstancia().addObjeto(eUsuario.getId(), usuario);
		
		return usuario;
	}

	public List<Usuario> getAll() {
		List<Entidad> entidades = servPersistencia.recuperarEntidades(USUARIO);

		List<Usuario> usuarios = new LinkedList<Usuario>();
		for (Entidad eUsuario : entidades) {
			usuarios.add(get(eUsuario.getId()));
		}
		
		return usuarios;
	}
	
	
	// -------------------Funciones auxiliares-----------------------------
			private String obtenerCodigosListaCanciones(List<ListaCanciones> listaCanciones) {
				String lineas = "";
				for (ListaCanciones listaCancion : listaCanciones) {
					lineas += listaCancion.getId() + " ";
				}
				return lineas.trim();

			}

			private List<ListaCanciones> obtenerListaCancionesDesdeCodigos(String lineas) {

				List<ListaCanciones> listaCanciones = new LinkedList<ListaCanciones>();
				if(lineas == null)
					return listaCanciones;
				StringTokenizer strTok = new StringTokenizer(lineas, " ");
				ListaCancionesDAO listaCancionesDAO;
				try {
					listaCancionesDAO = TDSFactoriaDAO.getInstancia().getListaCancionesDAO();
					while (strTok.hasMoreTokens()) {
						listaCanciones.add(listaCancionesDAO.get(Integer.valueOf((String) strTok.nextElement())));
					}
				} catch (DAOException e) {
					e.printStackTrace();
				}
				

				return listaCanciones;
			}
			
			private boolean esTrueOrFalse(String booleano) {
				if(booleano == null)
					return false;
				return booleano.equals("true");	
			}
			
			private String StringTrueOrFalse(boolean esPremium) {
				if(esPremium)
					return "true";
				return "false";
			}

}
