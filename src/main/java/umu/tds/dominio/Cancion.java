package umu.tds.dominio;

public class Cancion {
	
	private int id;
	private String titulo;
	private String interprete;
	private String estilo;
	private String URL;
	
	public Cancion(String titulo, String interprete, String estilo, String URL) {
		this.id = 0;
		this.titulo = titulo;
		this.interprete = interprete;
		this.estilo = estilo;
		this.URL = URL;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getInterprete() {
		return interprete;
	}

	public void setInterprete(String interprete) {
		this.interprete = interprete;
	}

	public String getEstilo() {
		return estilo;
	}

	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}
	
	
}
