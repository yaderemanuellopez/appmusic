package umu.tds.dominio;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import umu.tds.dao.DAOException;
import umu.tds.dao.FactoriaDAO;

public class CatalogoCanciones {
	private static CatalogoCanciones unicaInstancia;
	private FactoriaDAO factoria;

	private HashMap<Integer, Cancion> cancionesPorID;
	private HashMap<String, Cancion> cancionesPortitulo;

	public static CatalogoCanciones getUnicaInstancia() {
		if (unicaInstancia == null) unicaInstancia = new CatalogoCanciones();
		return unicaInstancia;
	}

	private CatalogoCanciones (){
		cancionesPorID = new HashMap<Integer, Cancion>();
		cancionesPortitulo = new HashMap<String, Cancion>();
		
		try {
			factoria = FactoriaDAO.getInstancia();
			
			List<Cancion> listaCanciones = factoria.getCancionDAO().getAll();
			for (Cancion cancion : listaCanciones) {
				cancionesPorID.put(cancion.getId(), cancion);
				cancionesPortitulo.put(cancion.getTitulo(), cancion);
			}
		} catch (DAOException eDAO) {
			   eDAO.printStackTrace();
		}
	}
	
	public List<Cancion> getCanciones() throws DAOException {
		return new LinkedList<Cancion>(cancionesPorID.values());
	}
	

	public Cancion getCancion(int id) {
		return cancionesPorID.get(id);
	}
	
	public Cancion getCancion(String titulo) {
		return cancionesPortitulo.get(titulo);
	}
	
	public void addCancion(Cancion cancion) {
		cancionesPorID.put(cancion.getId(), cancion);
	}
	
	public void removeCancion(Cancion cancion) {
		cancionesPorID.remove(cancion.getId());
	}

}
