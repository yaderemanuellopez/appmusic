package umu.tds.dominio;

import java.util.LinkedList;
import java.util.List;

public class ListaCanciones {

		private int id;
		private String nombre;
		private List<Cancion> canciones;
		
		public ListaCanciones(String nombre) {
			this.id = 0;
			this.nombre = nombre;
			this.canciones = new LinkedList<Cancion>();
		}
		
		public int getId() {
			return id;
		}



		public void setId(int id) {
			this.id = id;
		}



		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public List<Cancion> getCanciones() {
			return canciones;
		}

		public void setCanciones(List<Cancion> canciones) {
			this.canciones = canciones;
		}
		
		public void addCancion(Cancion cancion) {
			
			canciones.add(cancion);
			
		}
		
		
}
