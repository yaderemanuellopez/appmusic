package umu.tds.dominio;

import java.util.LinkedList;
import java.util.List;

import umu.tds.dao.DAOException;
import umu.tds.dao.FactoriaDAO;
import umu.tds.dao.ListaCancionesDAO;

public class Usuario {
	
	private int id;
	private String nombre;
	private String apellidos;
	private String email;
	private String login;
	private String password;
	private String fechaNacimiento;
	private boolean esPremium;
	private List<ListaCanciones> listaCanciones;

	public Usuario(String nombre, String apellidos, String email, String login, String password,
			String fechaNacimiento) {
		this.id = 0;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.email = email;
		this.login = login;
		this.password = password;
		this.fechaNacimiento = fechaNacimiento;
		this.esPremium = false;
		this.listaCanciones = new LinkedList<ListaCanciones>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public boolean isEsPremium() {
		return esPremium;
	}

	public void setEsPremium(boolean esPremium) {
		this.esPremium = esPremium;
	}

	public List<ListaCanciones> getListaCanciones() {
		return listaCanciones;
	}

	public void setListaCanciones(List<ListaCanciones> listaCanciones) {
		this.listaCanciones = listaCanciones;
	}
	

	public ListaCanciones addNuevaListaVacia(String nombreNuevaLista) {
		ListaCanciones nuevaListaVacia = new ListaCanciones(nombreNuevaLista);
		return nuevaListaVacia;
		
		
	}
	
	public void addNuevaLista(ListaCanciones nuevaLista) {
		this.listaCanciones.add(nuevaLista);
	}
	
	public boolean existeListaPorNombre(String nombreLista) {
		return listaCanciones.stream()
				.anyMatch(l -> l.getNombre().equals(nombreLista));
	}
	
	public ListaCanciones getListaCancionesPorNombre(String nombreLista) {
		for (ListaCanciones lista : listaCanciones) {
			if(lista.getNombre().equals(nombreLista))
				return lista;
		}
		return null;
	}
	

}
