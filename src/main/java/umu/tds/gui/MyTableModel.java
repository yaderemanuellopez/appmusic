package umu.tds.gui;

import javax.swing.table.DefaultTableModel;

public class MyTableModel extends DefaultTableModel {

		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

		public MyTableModel(Object[][] data, Object[] nombeColumnas) {
			super(data,nombeColumnas);
		}
		
		public MyTableModel() {
			super();
		}
		
		@Override
		public boolean isCellEditable(int row, int column) {
		return false;
		}
}
