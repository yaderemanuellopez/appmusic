package umu.tds.gui;

import javax.swing.JPanel;
import javax.swing.BoxLayout;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import javax.swing.table.AbstractTableModel;

import umu.tds.controlador.Controlador;
import umu.tds.dominio.*;

import javax.swing.JComboBox;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import java.awt.GridLayout;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;

public class PanelExplorar extends JPanel {
	private Controlador controlador;
	private JTextField textField;
	private JTextField textField_1;
	private JTable table;

	/**
	 * Create the panel.
	 */
	public PanelExplorar() {
		controlador = Controlador.getUnicaInstancia();//obtengo la instancia del controlador
		
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		JPanel panel_3 = new JPanel();
		add(panel_3);
		panel_3.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel_3.add(panel, BorderLayout.NORTH);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{5, 0, 300, 10, 0, 300, 10, 0, 0, 10, 0, 5, 0, 5, 0};
		gbl_panel.rowHeights = new int[]{10, 21, 5, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblNewLabel = new JLabel("Interprete:");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 1;
		panel.add(lblNewLabel, gbc_lblNewLabel);
		
		textField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.gridx = 2;
		gbc_textField.gridy = 1;
		panel.add(textField, gbc_textField);
		
		JLabel lblNewLabel_1 = new JLabel("Titulo:");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 4;
		gbc_lblNewLabel_1.gridy = 1;
		panel.add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		textField_1 = new JTextField();
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_1.insets = new Insets(0, 0, 5, 5);
		gbc_textField_1.gridx = 5;
		gbc_textField_1.gridy = 1;
		panel.add(textField_1, gbc_textField_1);
		
		JLabel lblNewLabel_2 = new JLabel("Estilo: ");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_2.gridx = 7;
		gbc_lblNewLabel_2.gridy = 1;
		panel.add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		JComboBox comboBox = new JComboBox();
		addItemsEstilos(comboBox);
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox.anchor = GridBagConstraints.NORTHWEST;
		gbc_comboBox.gridx = 8;
		gbc_comboBox.gridy = 1;
		panel.add(comboBox, gbc_comboBox);
		
		JButton btnAceptar = new JButton("Aceptar");
		GridBagConstraints gbc_btnAceptar = new GridBagConstraints();
		gbc_btnAceptar.insets = new Insets(0, 0, 5, 5);
		gbc_btnAceptar.gridx = 10;
		gbc_btnAceptar.gridy = 1;
		panel.add(btnAceptar, gbc_btnAceptar);
		
		JButton btnCancelar = new JButton("Cancelar");
		GridBagConstraints gbc_btnCancelar = new GridBagConstraints();
		gbc_btnCancelar.insets = new Insets(0, 0, 5, 5);
		gbc_btnCancelar.gridx = 12;
		gbc_btnCancelar.gridy = 1;
		panel.add(btnCancelar, gbc_btnCancelar);
		
		JPanel panel_1 = new JPanel();
		panel_3.add(panel_1, BorderLayout.SOUTH);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{5, 168, 168, 168, 168, 0, 0, 0, 0, 0};
		gbl_panel_1.rowHeights = new int[]{21, 0};
		gbl_panel_1.columnWeights = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JButton btnPlay = new JButton("New button");
		GridBagConstraints gbc_btnPlay = new GridBagConstraints();
		gbc_btnPlay.fill = GridBagConstraints.BOTH;
		gbc_btnPlay.insets = new Insets(0, 0, 0, 5);
		gbc_btnPlay.gridx = 1;
		gbc_btnPlay.gridy = 0;
		panel_1.add(btnPlay, gbc_btnPlay);
		
		JButton btnpause = new JButton("New button");
		GridBagConstraints gbc_btnpause = new GridBagConstraints();
		gbc_btnpause.fill = GridBagConstraints.BOTH;
		gbc_btnpause.insets = new Insets(0, 0, 0, 5);
		gbc_btnpause.gridx = 2;
		gbc_btnpause.gridy = 0;
		panel_1.add(btnpause, gbc_btnpause);
		
		JButton btnRetroceder = new JButton("New button");
		GridBagConstraints gbc_btnRetroceder = new GridBagConstraints();
		gbc_btnRetroceder.fill = GridBagConstraints.BOTH;
		gbc_btnRetroceder.insets = new Insets(0, 0, 0, 5);
		gbc_btnRetroceder.gridx = 3;
		gbc_btnRetroceder.gridy = 0;
		panel_1.add(btnRetroceder, gbc_btnRetroceder);
		
		JButton btnAdelantar = new JButton("New button");
		GridBagConstraints gbc_btnAdelantar = new GridBagConstraints();
		gbc_btnAdelantar.insets = new Insets(0, 0, 0, 5);
		gbc_btnAdelantar.fill = GridBagConstraints.BOTH;
		gbc_btnAdelantar.gridx = 4;
		gbc_btnAdelantar.gridy = 0;
		panel_1.add(btnAdelantar, gbc_btnAdelantar);
		
		JPanel panel_2 = new JPanel();
		panel_3.add(panel_2, BorderLayout.CENTER);
		
		Component rigidArea = Box.createRigidArea(new Dimension(20, 20));
		panel_2.add(rigidArea);
		
		 
		table = new JTable();
		MyTableModel miModelo =new MyTableModel();
		table.setModel(miModelo);
		panel_2.add(table);
		
		JScrollPane scrollPane = new JScrollPane(table);
		panel_2.add(scrollPane);
		
		Component rigidArea_1 = Box.createRigidArea(new Dimension(20, 20));
		panel_2.add(rigidArea_1);

		
		addManejadorBotonAceptar(btnAceptar, comboBox, textField_1, textField, table, miModelo);
		
		
	}
	
	
	
	private void addManejadorBotonAceptar(JButton btnAceptar,JComboBox comboBox,JTextField titulo,JTextField interprete,JTable tabla,MyTableModel miModelo) {
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				List<Cancion> canciones;
				String estilo = (String)comboBox.getSelectedItem();
				if(estilo.equals("Todas")) 
					canciones = controlador.getAllcanciones();
				else
					canciones = controlador.getCacionesporfiltro(titulo.getText(), interprete.getText(),(String)comboBox.getSelectedItem());
				miModelo.setCanciones(canciones);
				miModelo.fireTableDataChanged();
				
			}
		});
	}
	
	public void addItemsEstilos(JComboBox comboBox) {
		for (String estilo : controlador.getEstilosMusicales()) {
			comboBox.addItem(estilo);
		}
		comboBox.addItem("Todas");
	}
	
	
	
	class MyTableModel extends AbstractTableModel {
		 private String[] columnNames = {"Titulo","Interprete"};
		 private ListaCanciones listacanciones;
		 private List<Cancion> canciones;

		 public MyTableModel() {
			 canciones = controlador.getAllcanciones();
		}
		 public int getColumnCount() {
		 return columnNames.length;
		 }
		 public int getRowCount() {
		 return canciones.size();
		 }
		 public String getColumnName(int col) {
		 return columnNames[col];
		 }
		 public Object getValueAt(int row, int col) {
		 String valor = "";
		 Cancion cancion = canciones.get(row);
		 switch (col) {
		case 0:
			valor = cancion.getTitulo();
			break;
		case 1:
			valor = cancion.getInterprete();
			break;
		default:
			break;}
		 return valor;
		 }
		 
		 public void setCanciones(List<Cancion> canciones) {
			this.canciones = canciones;
		}
	}



}
