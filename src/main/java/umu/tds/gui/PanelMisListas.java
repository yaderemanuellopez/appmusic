package umu.tds.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.AbstractListModel;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

import umu.tds.controlador.Controlador;
import umu.tds.dominio.Cancion;
import umu.tds.dominio.ListaCanciones;
import umu.tds.gui.MyTableModel;

import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import java.awt.event.ActionEvent;

public class PanelMisListas extends JPanel {
	private Controlador controlador;
	private JTable table;
	private JTable table_1;

	/**
	 * Create the panel.
	 */
	public PanelMisListas() {
		controlador = Controlador.getUnicaInstancia();//obtengo la instancia del controlador
		
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		JPanel panel_3 = new JPanel();
		add(panel_3);
		panel_3.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		panel_3.add(panel_1, BorderLayout.SOUTH);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{8, 0, 8, 0, 8, 0, 8, 0, 8, 0};
		gbl_panel_1.rowHeights = new int[]{21, 0};
		gbl_panel_1.columnWeights = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JButton btnPlay = new JButton("play");
		GridBagConstraints gbc_btnPlay = new GridBagConstraints();
		gbc_btnPlay.insets = new Insets(0, 0, 0, 5);
		gbc_btnPlay.gridx = 1;
		gbc_btnPlay.gridy = 0;
		panel_1.add(btnPlay, gbc_btnPlay);
		
		JButton btnPause = new JButton("Pause");
		btnPause.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		GridBagConstraints gbc_btnPause = new GridBagConstraints();
		gbc_btnPause.insets = new Insets(0, 0, 0, 5);
		gbc_btnPause.gridx = 3;
		gbc_btnPause.gridy = 0;
		panel_1.add(btnPause, gbc_btnPause);
		
		JButton btnSiguiente = new JButton("next");
		GridBagConstraints gbc_btnSiguiente = new GridBagConstraints();
		gbc_btnSiguiente.fill = GridBagConstraints.VERTICAL;
		gbc_btnSiguiente.insets = new Insets(0, 0, 0, 5);
		gbc_btnSiguiente.gridx = 5;
		gbc_btnSiguiente.gridy = 0;
		panel_1.add(btnSiguiente, gbc_btnSiguiente);
		
		JButton btnAnterior = new JButton("anterior");
		GridBagConstraints gbc_btnAnterior = new GridBagConstraints();
		gbc_btnAnterior.fill = GridBagConstraints.VERTICAL;
		gbc_btnAnterior.insets = new Insets(0, 0, 0, 5);
		gbc_btnAnterior.gridx = 7;
		gbc_btnAnterior.gridy = 0;
		panel_1.add(btnAnterior, gbc_btnAnterior);
		
		JPanel panel_2 = new JPanel();
		panel_3.add(panel_2, BorderLayout.CENTER);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.X_AXIS));
		
		table_1 = new JTable();
		panel_2.add(table_1);
		
		JScrollPane scrollPane = new JScrollPane(table_1);
		panel_2.add(scrollPane);
		
		JPanel panel_7 = new JPanel();
		panel_3.add(panel_7, BorderLayout.WEST);
		panel_7.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		Object[] nom = {"id","nombre"};
		Object[][] row = {{"2","yader"},{"1","nombre"}};
		MyTableModel modelo = new MyTableModel(row, nom);
		JTable tabla_3 = new JTable();
		tabla_3.setModel(modelo);
		
		tabla_3.setModel(new MyTableModel());
		
		panel_7.add(tabla_3);
		
		JScrollPane scrollPane_2 = new JScrollPane(tabla_3);
		panel_7.add(scrollPane_2);
		
	}
	
	

}
