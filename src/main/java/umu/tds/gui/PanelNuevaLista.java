package umu.tds.gui;

import javax.swing.JPanel;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import umu.tds.controlador.Controlador;
import umu.tds.dominio.Cancion;
import umu.tds.dominio.ListaCanciones;
import umu.tds.gui.PanelExplorar.MyTableModel;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.AbstractListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import java.awt.Component;
import javax.swing.JList;
import javax.swing.JTable;
import java.awt.GridLayout;
import java.awt.Dimension;

public class PanelNuevaLista extends JPanel {
	Controlador controlador;
	private JFrame ventana;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTable table;
	private JTable table_1;
	private JTable table_2;

	/**
	 * Create the panel.
	 */
	public PanelNuevaLista(JFrame ventana) {
		controlador = Controlador.getUnicaInstancia();
		this.ventana = ventana;
		setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		add(panel, BorderLayout.NORTH);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{8, 0, 8, 200, 8, 0, 8, 0};
		gbl_panel.rowHeights = new int[]{8, 19, 8, 0};
		gbl_panel.columnWeights = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblNewLabel = new JLabel("Nueva Lista: ");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 1;
		panel.add(lblNewLabel, gbc_lblNewLabel);
		
		textField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.anchor = GridBagConstraints.NORTH;
		gbc_textField.gridx = 3;
		gbc_textField.gridy = 1;
		panel.add(textField, gbc_textField);
		textField.setColumns(10);
		
		JButton btnCrear = new JButton("Crear");
		GridBagConstraints gbc_btnCrear = new GridBagConstraints();
		gbc_btnCrear.insets = new Insets(0, 0, 5, 5);
		gbc_btnCrear.gridx = 5;
		gbc_btnCrear.gridy = 1;
		panel.add(btnCrear, gbc_btnCrear);
		
		
		JPanel panelCrear = new JPanel();
		add(panelCrear, BorderLayout.CENTER);
		panelCrear.setVisible(false);
		GridBagLayout gbl_panelCrear = new GridBagLayout();
		gbl_panelCrear.columnWidths = new int[]{0, 0};
		gbl_panelCrear.rowHeights = new int[]{0, 0, 0};
		gbl_panelCrear.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panelCrear.rowWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		panelCrear.setLayout(gbl_panelCrear);
		
		JPanel panel_4 = new JPanel();
		GridBagConstraints gbc_panel_4 = new GridBagConstraints();
		gbc_panel_4.insets = new Insets(0, 0, 5, 0);
		gbc_panel_4.fill = GridBagConstraints.BOTH;
		gbc_panel_4.gridx = 0;
		gbc_panel_4.gridy = 0;
		panelCrear.add(panel_4, gbc_panel_4);
		GridBagLayout gbl_panel_4 = new GridBagLayout();
		gbl_panel_4.columnWidths = new int[]{5, 0, 300, 10, 0, 300, 10, 0, 0, 10, 0, 5, 0, 5, 0};
		gbl_panel_4.rowHeights = new int[]{10, 21, 5, 0};
		gbl_panel_4.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_4.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_4.setLayout(gbl_panel_4);
		
		JLabel lblNewLabel_1 = new JLabel("Interprete:");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 1;
		gbc_lblNewLabel_1.gridy = 1;
		panel_4.add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		textField_1 = new JTextField();
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_1.insets = new Insets(0, 0, 5, 5);
		gbc_textField_1.gridx = 2;
		gbc_textField_1.gridy = 1;
		panel_4.add(textField_1, gbc_textField_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("Titulo:");
		GridBagConstraints gbc_lblNewLabel_1_1 = new GridBagConstraints();
		gbc_lblNewLabel_1_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblNewLabel_1_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1_1.gridx = 4;
		gbc_lblNewLabel_1_1.gridy = 1;
		panel_4.add(lblNewLabel_1_1, gbc_lblNewLabel_1_1);
		
		textField_2 = new JTextField();
		GridBagConstraints gbc_textField_2 = new GridBagConstraints();
		gbc_textField_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_2.insets = new Insets(0, 0, 5, 5);
		gbc_textField_2.gridx = 5;
		gbc_textField_2.gridy = 1;
		panel_4.add(textField_2, gbc_textField_2);
		
		JLabel lblNewLabel_2 = new JLabel("Estilo: ");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 7;
		gbc_lblNewLabel_2.gridy = 1;
		panel_4.add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		JComboBox comboBox = new JComboBox();
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.anchor = GridBagConstraints.NORTHWEST;
		gbc_comboBox.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox.gridx = 8;
		gbc_comboBox.gridy = 1;
		panel_4.add(comboBox, gbc_comboBox);
		
		JButton btnBuscar = new JButton("Buscar");
		GridBagConstraints gbc_btnBuscar = new GridBagConstraints();
		gbc_btnBuscar.insets = new Insets(0, 0, 5, 5);
		gbc_btnBuscar.gridx = 10;
		gbc_btnBuscar.gridy = 1;
		panel_4.add(btnBuscar, gbc_btnBuscar);
		
		JPanel panel_2 = new JPanel();
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.gridx = 0;
		gbc_panel_2.gridy = 1;
		panelCrear.add(panel_2, gbc_panel_2);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[]{0, 0, 0};
		gbl_panel_2.rowHeights = new int[]{0, 0, 0};
		gbl_panel_2.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		gbl_panel_2.rowWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		panel_2.setLayout(gbl_panel_2);
		
		JPanel panel_3 = new JPanel();
		GridBagConstraints gbc_panel_3 = new GridBagConstraints();
		gbc_panel_3.insets = new Insets(0, 0, 5, 5);
		gbc_panel_3.fill = GridBagConstraints.BOTH;
		gbc_panel_3.gridx = 0;
		gbc_panel_3.gridy = 0;
		panel_2.add(panel_3, gbc_panel_3);
		
		MyTableListaModel miModeloLista = new MyTableListaModel();
		table_2 = new JTable();
		table_2.setMaximumSize(new Dimension(50, 0));
		table_2.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table_2.setModel(miModeloLista);
		panel_3.add(table_2);
		
		JScrollPane scrollPane = new JScrollPane(table_2);
		scrollPane.setMaximumSize(new Dimension(32768, 32768));
		panel_3.add(scrollPane);
		
	
		
		JPanel panel_5 = new JPanel();
		GridBagConstraints gbc_panel_5 = new GridBagConstraints();
		gbc_panel_5.insets = new Insets(0, 0, 5, 0);
		gbc_panel_5.fill = GridBagConstraints.BOTH;
		gbc_panel_5.gridx = 1;
		gbc_panel_5.gridy = 0;
		panel_2.add(panel_5, gbc_panel_5);
		panel_5.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_6 = new JPanel();
		panel_5.add(panel_6, BorderLayout.WEST);
		
		MyTableModel miModelo1 = new MyTableModel();
		table = new JTable();
		table.setModel(miModelo1);
		panel_6.add(table);
		
		JScrollPane scrollPane_1 = new JScrollPane(table);
		panel_6.add(scrollPane_1);
		
		JPanel panel_7 = new JPanel();
		panel_5.add(panel_7, BorderLayout.EAST);
		
		MyTableModel miModelo2 = new MyTableModel();
		miModelo2.VaciarLista();
		table_1 = new JTable();
		table_1.setModel(miModelo2);
		panel_7.add(table_1);
		
		JScrollPane scrollPane_2 = new JScrollPane(table_1);
		panel_7.add(scrollPane_2);
		
		JPanel panel_8 = new JPanel();
		panel_5.add(panel_8, BorderLayout.CENTER);
		GridBagLayout gbl_panel_8 = new GridBagLayout();
		gbl_panel_8.columnWidths = new int[]{0, 0};
		gbl_panel_8.rowHeights = new int[]{5, 0, 10, 0, 5, 0};
		gbl_panel_8.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gbl_panel_8.rowWeights = new double[]{1.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		panel_8.setLayout(gbl_panel_8);
		
		JButton btnanadir = new JButton(">>");
		GridBagConstraints gbc_btnanadir = new GridBagConstraints();
		gbc_btnanadir.insets = new Insets(0, 0, 5, 0);
		gbc_btnanadir.gridx = 0;
		gbc_btnanadir.gridy = 1;
		panel_8.add(btnanadir, gbc_btnanadir);
		
		JButton btneliminar = new JButton("<<");
		GridBagConstraints gbc_btneliminar = new GridBagConstraints();
		gbc_btneliminar.insets = new Insets(0, 0, 5, 0);
		gbc_btneliminar.gridx = 0;
		gbc_btneliminar.gridy = 3;
		panel_8.add(btneliminar, gbc_btneliminar);
		
		
		
		
		JPanel panel_1 = new JPanel();
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.gridwidth = 2;
		gbc_panel_1.insets = new Insets(0, 0, 0, 5);
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 1;
		panel_2.add(panel_1, gbc_panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{10, 0, 10, 0, 10, 0, 0, 0, 0, 0, 0};
		gbl_panel_1.rowHeights = new int[]{10, 0, 10, 0};
		gbl_panel_1.columnWeights = new double[]{1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JButton btnAceptar = new JButton("Aceptar");
		GridBagConstraints gbc_btnAceptar = new GridBagConstraints();
		gbc_btnAceptar.insets = new Insets(0, 0, 5, 5);
		gbc_btnAceptar.gridx = 1;
		gbc_btnAceptar.gridy = 1;
		panel_1.add(btnAceptar, gbc_btnAceptar);
		
		JButton btnCancelar = new JButton("Cancelar");
		GridBagConstraints gbc_btnCancelar = new GridBagConstraints();
		gbc_btnCancelar.insets = new Insets(0, 0, 5, 5);
		gbc_btnCancelar.gridx = 3;
		gbc_btnCancelar.gridy = 1;
		panel_1.add(btnCancelar, gbc_btnCancelar);
		
		
		addManejadorBotonBuscar(btnBuscar, comboBox, textField_2, textField_1, table, miModelo1);
		addManejadorBotonCrear(btnCrear, panelCrear, ventana, miModelo2);
		addManejadorBotonAceptar(btnAceptar, miModelo2,miModeloLista);
		addItemsEstilos(comboBox);
		
	}
	

	
	private void addManejadorBotonCrear(JButton btnCrear,JPanel panelCrear,JFrame ventana,MyTableModel miModelo) {
		btnCrear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int res = JOptionPane.showConfirmDialog(ventana,"desea crear una nueva lista?","Nueva lista",JOptionPane.YES_NO_OPTION);
				if(res == JOptionPane.YES_OPTION && !textField.getText().isEmpty()) {
					miModelo.setListacanciones(controlador.NuevaListaVacia(textField.getText()));
					if(!panelCrear.isVisible()) {
						panelCrear.setVisible(true);
						repaint();
						validate();
						ventana.pack();
					}
					
				}
				
				if(textField.getText().isEmpty() && res == JOptionPane.YES_OPTION)
					JOptionPane.showMessageDialog(ventana, "No se ha introducido un nombre para la lista");

			}
		});
	}
	
	
	public void addItemsEstilos(JComboBox comboBox) {
		for (String estilo : controlador.getEstilosMusicales()) {
			comboBox.addItem(estilo);
		}
	}
	
	private void addManejadorBotonBuscar(JButton btnBuscar,JComboBox comboBox,JTextField titulo,JTextField interprete,JTable tabla,MyTableModel miModelo) {
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				List<Cancion> canciones = controlador.getCacionesporfiltro(titulo.getText(), interprete.getText(),(String)comboBox.getSelectedItem());
				System.out.println(titulo.getText());
				System.out.println(interprete.getText());
				System.out.println((String)comboBox.getSelectedItem());
				miModelo.setCanciones(canciones);
				miModelo.fireTableDataChanged();
			}
		});
	}
	
	private void addManejadorMisListas(JList lista,MyTableModel miModelo) {
		lista.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent event) {
				if (!event.getValueIsAdjusting()) {
					JList source = (JList)event.getSource();
					miModelo.setListacanciones((ListaCanciones)source.getSelectedValue());
					
				}
			}
		});
	}
	
	private void addManejadorBotonAceptar(JButton btnAceptar,MyTableModel miModelo,MyTableListaModel miModeloLista) {
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				controlador.NuevaLista(miModelo.getListacanciones());
				miModeloLista.setListaCanciones(controlador.getMisListas());
				miModeloLista.fireTableDataChanged();
			}
		});
	}

	class MyListModel extends AbstractListModel{
		
		List<ListaCanciones> values;

		public MyListModel() {
			values = controlador.getMisListas();
		}
		public int getSize() {
			return values.size();
		}

		public Object getElementAt(int index) {
			return values.get(index).getNombre();
		}
		
		public void addLista(ListaCanciones lista){
			  values.add(lista);
			  this.fireIntervalAdded(this, getSize(), getSize()+1);
			 }
		
		public void setListaCanciones(List<ListaCanciones> listaCanciones) {
			values = listaCanciones;
		}
		
}
	
	class MyTableModel extends AbstractTableModel {
		 private String[] columnNames = {"Titulo","Interprete"};
		 private ListaCanciones listacanciones;
		 private List<Cancion> canciones;

		 public MyTableModel() {
			 canciones = controlador.getAllcanciones();
		}
		 public int getColumnCount() {
		 return columnNames.length;
		 }
		 public int getRowCount() {
		 return canciones.size();
		 }
		 public String getColumnName(int col) {
		 return columnNames[col];
		 }
		 public Object getValueAt(int row, int col) {
		 String valor = "";
		 Cancion cancion = canciones.get(row);
		 switch (col) {
		case 0:
			valor = cancion.getTitulo();
			break;
		case 1:
			valor = cancion.getInterprete();
			break;
		default:
			break;}
		 return valor;
		 }
		 
		 public void setListacanciones(ListaCanciones listacanciones) {
			this.listacanciones = listacanciones;
			canciones = listacanciones.getCanciones();
		}
		 
		public ListaCanciones getListacanciones() {
			return listacanciones;
		}
		 
		 public void setCanciones(List<Cancion> canciones) {
			this.canciones = canciones;
		}
		 
		 public void VaciarLista() {
			 canciones.removeAll(canciones);
		 }
	}
	
	
	
	class MyTableListaModel extends AbstractTableModel {
		private String[] columnNames = { "Playlist" };
		private List<ListaCanciones> listaCanciones;

		public MyTableListaModel() {
			listaCanciones = controlador.getMisListas();
		}

		public int getColumnCount() {
			return columnNames.length;
		}

		public int getRowCount() {
			return listaCanciones.size();
		}

		public String getColumnName(int col) {
			return columnNames[col];
		}

		public Object getValueAt(int row, int col) {
			String valor = "";
			ListaCanciones listaCanciones = this.listaCanciones.get(row);
			switch (col) {
			case 0:
				valor = listaCanciones.getNombre();
				break;
			default:
				break;
			}
			return valor;
		}

		public void setListaCanciones(List<ListaCanciones> listacanciones) {
			this.listaCanciones = listacanciones;
		}
	}

}
