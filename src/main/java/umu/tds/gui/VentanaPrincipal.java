package umu.tds.gui;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.Font;
import javax.swing.border.LineBorder;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.awt.event.ActionEvent;
import pulsador.Luz;
import umu.tds.controlador.Controlador;

public class VentanaPrincipal implements PropertyChangeListener{

	private JFrame frmVentanaPrincipal;
	private Controlador controlador;


	public void mostrarVentana() {
		frmVentanaPrincipal.setLocationRelativeTo(null);
		frmVentanaPrincipal.setVisible(true);
	}


	public VentanaPrincipal() {
		initialize();
	}

	private void initialize() {
		controlador = Controlador.getUnicaInstancia();
		frmVentanaPrincipal = new JFrame();
		frmVentanaPrincipal.setResizable(false);
		frmVentanaPrincipal.getContentPane().setFont(new Font("Times New Roman", Font.PLAIN, 14));
		frmVentanaPrincipal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmVentanaPrincipal.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		frmVentanaPrincipal.getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{5, 0, 0, 0, 0, 200, 8, 15, 8, 0, 8, 0, 8, 0, 8, 0};
		gbl_panel_1.rowHeights = new int[]{0, 5, 5, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/umu/tds/imagenes/LogoVentanaPrincipal.png")));
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 1;
		panel_1.add(lblNewLabel, gbc_lblNewLabel);
		
		Component rigidArea = Box.createRigidArea(new Dimension(20, 20));
		GridBagConstraints gbc_rigidArea = new GridBagConstraints();
		gbc_rigidArea.gridwidth = 5;
		gbc_rigidArea.insets = new Insets(0, 0, 5, 5);
		gbc_rigidArea.gridx = 2;
		gbc_rigidArea.gridy = 1;
		panel_1.add(rigidArea, gbc_rigidArea);
		
		Color colorLuz =new Color(255,255,0);
		
		Luz luz = new Luz();
		luz.addEncendidoListener(this);
		GridBagConstraints gbc_luz = new GridBagConstraints();
		gbc_luz.insets = new Insets(0, 0, 5, 5);
		gbc_luz.gridx = 7;
		gbc_luz.gridy = 1;
		luz.setColor(colorLuz);
		panel_1.add(luz, gbc_luz);
		
		JLabel lblBienvenida = new JLabel("Hola Usuario");
		lblBienvenida.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		GridBagConstraints gbc_lblBienvenida = new GridBagConstraints();
		gbc_lblBienvenida.anchor = GridBagConstraints.EAST;
		gbc_lblBienvenida.insets = new Insets(0, 0, 5, 5);
		gbc_lblBienvenida.gridx = 9;
		gbc_lblBienvenida.gridy = 1;
		panel_1.add(lblBienvenida, gbc_lblBienvenida);
		
		JButton btnMejorarcuenta = new JButton("Pasa al siguiente nivel");
		btnMejorarcuenta.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		GridBagConstraints gbc_btnMejorarcuenta = new GridBagConstraints();
		gbc_btnMejorarcuenta.insets = new Insets(0, 0, 5, 5);
		gbc_btnMejorarcuenta.gridx = 11;
		gbc_btnMejorarcuenta.gridy = 1;
		panel_1.add(btnMejorarcuenta, gbc_btnMejorarcuenta);
		
		JButton btnSalir = new JButton("Salir");
		addManejadorBotonSalir(btnSalir);
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnSalir.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnSalir.setForeground(Color.RED);
		GridBagConstraints gbc_btnSalir = new GridBagConstraints();
		gbc_btnSalir.insets = new Insets(0, 0, 5, 5);
		gbc_btnSalir.gridx = 13;
		gbc_btnSalir.gridy = 1;
		panel_1.add(btnSalir, gbc_btnSalir);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		panel.add(panel_2);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[]{5, 0, 5, 0, 5, 0, 5, 0, 0};
		gbl_panel_2.rowHeights = new int[]{0, 0};
		gbl_panel_2.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_2.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panel_2.setLayout(gbl_panel_2);
		
		JButton btnExplorar = new JButton("Explorar");
		

		btnExplorar.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/umu/tds/imagenes/IconosExplorar.png")));
		btnExplorar.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		GridBagConstraints gbc_btnExplorar = new GridBagConstraints();
		gbc_btnExplorar.insets = new Insets(0, 0, 0, 5);
		gbc_btnExplorar.gridx = 1;
		gbc_btnExplorar.gridy = 0;
		panel_2.add(btnExplorar, gbc_btnExplorar);
		
		JButton btnMisListas = new JButton("Mis listas");
		btnMisListas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnMisListas.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/umu/tds/imagenes/IconoMisListas.png")));
		btnMisListas.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		GridBagConstraints gbc_btnMisListas = new GridBagConstraints();
		gbc_btnMisListas.insets = new Insets(0, 0, 0, 5);
		gbc_btnMisListas.gridx = 3;
		gbc_btnMisListas.gridy = 0;
		panel_2.add(btnMisListas, gbc_btnMisListas);
		
		JButton btnNuevaLista = new JButton("Nueva lista");
		
		btnNuevaLista.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/umu/tds/imagenes/IconoNuevaLista.png")));
		btnNuevaLista.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		GridBagConstraints gbc_btnNuevaLista = new GridBagConstraints();
		gbc_btnNuevaLista.insets = new Insets(0, 0, 0, 5);
		gbc_btnNuevaLista.gridx = 5;
		gbc_btnNuevaLista.gridy = 0;
		panel_2.add(btnNuevaLista, gbc_btnNuevaLista);
		
		JButton btnRecientes = new JButton("Recientes");
		btnRecientes.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/umu/tds/imagenes/IconoReciente.png")));
		btnRecientes.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		GridBagConstraints gbc_btnRecientes = new GridBagConstraints();
		gbc_btnRecientes.gridx = 7;
		gbc_btnRecientes.gridy = 0;
		panel_2.add(btnRecientes, gbc_btnRecientes);
		
	
		//*********************************Paneles centrales****************************************//
		
		JPanel panelCentral = new JPanel();
		frmVentanaPrincipal.getContentPane().add(panelCentral, BorderLayout.CENTER);
		
		JPanel panelBienvenido = new JPanel();
		panelCentral.add(panelBienvenido);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/umu/tds/imagenes/Bienvenido.png")));
		panelBienvenido.add(lblNewLabel_1);
		
		JPanel panelExplorar = new PanelExplorar();
		JPanel panelNuevaLista = new PanelNuevaLista(frmVentanaPrincipal);
		JPanel panelMisListas = new PanelMisListas();

		//******************************************************************************************//
		
		addManejadorBotonExplorar(btnExplorar, panelCentral, panelBienvenido, panelExplorar, panelNuevaLista, panelMisListas);
		addManejadorBotonMisListas(btnMisListas, panelCentral, panelBienvenido, panelExplorar, panelNuevaLista, panelMisListas);
		addManejadorBotonNuevaLista(btnNuevaLista, panelCentral, panelBienvenido, panelExplorar, panelNuevaLista, panelMisListas);
		addManejadorBotonRecientes(btnRecientes, panelCentral, panelBienvenido, panelExplorar, panelNuevaLista, panelMisListas);

		frmVentanaPrincipal.pack();
	}
	
	
	private void addManejadorBotonSalir(JButton btnSalir) {
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmVentanaPrincipal.dispose();
				LoginView ventana = new LoginView();
				ventana.mostrarVentana();
			}
		});
	}
	
	private void addManejadorBotonExplorar(JButton btnExplorar,JPanel panelCentral, JPanel panelBienvenido, JPanel panelExplorar, JPanel panelNuevaLista, JPanel panelMisListas) {
		btnExplorar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panelCentral.remove(panelBienvenido);
				panelCentral.remove(panelMisListas);
				panelCentral.remove(panelNuevaLista);
				panelCentral.add(panelExplorar, BorderLayout.CENTER);
				frmVentanaPrincipal.repaint();
				frmVentanaPrincipal.validate();
				frmVentanaPrincipal.pack();
			}
		});
	}
	
	private void addManejadorBotonMisListas(JButton btnMisListas,JPanel panelCentral, JPanel panelBienvenido, JPanel panelExplorar, JPanel panelNuevaLista, JPanel panelMisListas) {
		btnMisListas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panelCentral.remove(panelBienvenido);
				panelCentral.remove( panelNuevaLista);
				panelCentral.remove(panelExplorar);
				panelCentral.add(panelMisListas, BorderLayout.CENTER);
				frmVentanaPrincipal.repaint();
				frmVentanaPrincipal.validate();
				frmVentanaPrincipal.pack();
			}
		});
	}
	
	private void addManejadorBotonNuevaLista(JButton btnNuevaLista,JPanel panelCentral, JPanel panelBienvenido, JPanel panelExplorar, JPanel panelNuevaLista, JPanel panelMisListas) {
		btnNuevaLista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panelCentral.remove(panelBienvenido);
				panelCentral.remove(panelMisListas);
				panelCentral.remove(panelExplorar);
				panelCentral.add(panelNuevaLista, BorderLayout.CENTER);
				frmVentanaPrincipal.repaint();
				frmVentanaPrincipal.validate();
				frmVentanaPrincipal.pack();
			}
		});
	}
	
	private void addManejadorBotonRecientes(JButton btnrecientes,JPanel panelCentral, JPanel panelBienvenido, JPanel panelExplorar, JPanel panelNuevaLista, JPanel panelMisListas) {
		btnrecientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panelCentral.remove(panelBienvenido);
				panelCentral.remove( panelNuevaLista);
				panelCentral.remove(panelExplorar);
				panelCentral.add(panelMisListas, BorderLayout.CENTER);
				frmVentanaPrincipal.repaint();
				frmVentanaPrincipal.validate();
				frmVentanaPrincipal.pack();
			}
		});
	}


	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if((boolean)evt.getNewValue() == true) {
			JFileChooser chooser = new JFileChooser();
			chooser.showOpenDialog(frmVentanaPrincipal);
			File currentFile = chooser.getSelectedFile();
			if(currentFile!=null)
				controlador.cargarCanciones(currentFile.getPath());
				
		}
	}
	


}
